import {PersonalFormDetails} from "../personalFormDetails.interface";

export interface PlayerGateway {
    retrievePersonalFormDetails(playerId: number): Promise<PersonalFormDetails | undefined>
}