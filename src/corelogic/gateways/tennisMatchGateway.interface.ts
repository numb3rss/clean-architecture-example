import {TennisMatch} from "../tennisMatch.interface";

export interface TennisMatchGateway {
    giveAPoint(player: number): Promise<TennisMatch>
}
