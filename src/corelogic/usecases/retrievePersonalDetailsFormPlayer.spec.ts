import { configureStore, ReduxStore } from "../redux/reduxStoreConfiguration";
import { AppState } from "../redux/appState.interface";
import { retrievePersonalDetailsFormPlayer } from "./retrievePersonalDetailsFormPlayer";
import {InMemoryPlayerGateway} from "../../adapters/secondary/inMemoryPlayerGateway";

describe('retrieve personal details form for a player', () => {
    const personalDetailsForm = {
        age: 34,
        birthPlace: 'Manacor, Espagne',
        size: 1.85,
        weight: 84
    };
    let store: ReduxStore;
    let initialState: AppState;
    let playerGateway: InMemoryPlayerGateway;

    beforeEach(() => {
        playerGateway = new InMemoryPlayerGateway()
        store = configureStore({ playerGateway });
        initialState = store.getState();
    });

    it('should track the retrieval', (done) => {
        subscribeOnReduxState(store, done, () => {
            expect(store.getState()).toEqual({
                ...initialState,
                player: {
                    personalDetailsForm: {},
                    fetching: true
                }
            });
        });
        retrievePersonalDetailsForm(1);
    });

    it('retrieve personal details form for Nadal', async () => {
        const playerId = 1;
        playerGateway.feedData();
        await retrievePersonalDetailsForm(playerId);
        expect(store.getState()).toEqual({
           ...initialState,
            player: {
               personalDetailsForm,
                fetching: false
            }
        });
    })

    const retrievePersonalDetailsForm = (playerId: number) => {
        return store.dispatch(retrievePersonalDetailsFormPlayer(playerId));
    }

    const subscribeOnReduxState = (store: ReduxStore,
                                   done: jest.DoneCallback,
                                   expectation: () => void) => {
        const unsubscribe = store.subscribe(() => {
            try {
                expectation();
                done();
            } catch (e) {
                done(e);
            } finally {
                unsubscribe();
            }
        });
    };
})