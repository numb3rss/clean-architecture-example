import {ThunkResult} from "../redux/reduxStoreConfiguration";
import {
    retrievePersonalDetailsFormFetching,
    retrievePersonalDetailsFormPlayerRetrieved
} from "../redux/player/playerActions";
import {PlayerGateway} from "../gateways/playerGateway.interface";

export const retrievePersonalDetailsFormPlayer = (playerId: number): ThunkResult<Promise<void>> => async (dispatch,
                                                                                                          getState,
                                                                                                          {playerGateway}: {playerGateway: PlayerGateway}) => {
    dispatch(retrievePersonalDetailsFormFetching());
    const personalDetailsForm = await playerGateway.retrievePersonalFormDetails(playerId);
    dispatch(retrievePersonalDetailsFormPlayerRetrieved(personalDetailsForm));
};
