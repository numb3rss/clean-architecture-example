import { ThunkResult } from "../redux/reduxStoreConfiguration";
import { TennisMatchGateway } from "../gateways/tennisMatchGateway.interface";
import { retrieveTennisMatchScore, tennisMatchScoreRetrieved } from "../redux/tennismatch/tennisMatchActions";

export const giveAPointToWinningPlayer = (player: number): ThunkResult<Promise<void>> => async (dispatch,
                                                                                                getState,
                                                                                                {tennisMatchGateway}: { tennisMatchGateway: TennisMatchGateway }) => {
    dispatch(retrieveTennisMatchScore());
    const match = await tennisMatchGateway.giveAPoint(player);
    dispatch(tennisMatchScoreRetrieved(match));
};
