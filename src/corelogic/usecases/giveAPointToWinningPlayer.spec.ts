import {AppState} from "../redux/appState.interface";
import {configureStore, ReduxStore} from "../redux/reduxStoreConfiguration";
import {InMemoryTennisMatchGateway} from "../../adapters/secondary/inMemoryTennisMatchGateway";
import {giveAPointToWinningPlayer} from "./giveAPointToWinningPlayer";

describe('Compute the score of a tennis match', () => {

    let store: ReduxStore;
    let initialState: AppState;
    let tennisMatchGateway: InMemoryTennisMatchGateway;

    beforeEach(() => {
        tennisMatchGateway = new InMemoryTennisMatchGateway();
        store = configureStore({tennisMatchGateway});
        initialState = store.getState();
    });

    it('should track the retrieval', (done) => {
        subscribeOnReduxState(store, done, () => {
            expect(store.getState()).toEqual({
                ...initialState,
                tennisMatch: {
                    label: '',
                    score: {},
                    fetching: true
                }
            });
        });
        giveAPoint(1);
    });

    it('compute the score of a match according to the current winning point', async () => {
        feedWithTennisMatchScore();
        await giveAPoint(1);
        expect(tennisMatchGateway.justWonPlayer).toBe(1);
        expect(store.getState()).toEqual({
            ...initialState,
            tennisMatch: {
                label: 'Nadal VS Federer',
                score: {
                    'Nadal': {
                        games: 0,
                        points: '15'
                    },
                    'Federer': {
                        games: 0,
                        points: '0'
                    }
                },
                fetching: false
            }
        });
    });

    const feedWithTennisMatchScore = () => {
        tennisMatchGateway.feedWithTennisMatch({
            label: 'Nadal VS Federer',
            score: {
                'Nadal': {
                    games: 0,
                    points: '15'
                },
                'Federer': {
                    games: 0,
                    points: '0'
                }
            },
        });
    }

    const giveAPoint = (player: number) => {
        return store.dispatch(giveAPointToWinningPlayer(player));
    }

    const subscribeOnReduxState = (store: ReduxStore,
                                   done: jest.DoneCallback,
                                   expectation: () => void) => {
        const unsubscribe = store.subscribe(() => {
            try {
                expectation();
                done();
            } catch (e) {
                done(e);
            } finally {
                unsubscribe();
            }
        });
    };
})
;
