export interface TennisMatch {
    label: string,
    score: {
        [player: string]: {
            games: number,
            points: string
        }
    }
}
