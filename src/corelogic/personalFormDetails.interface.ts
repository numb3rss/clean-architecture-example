export interface PersonalFormDetails {
    age: number,
    birthPlace: string,
    size: number,
    weight: number
}