import { TennisMatchGateway } from "../gateways/tennisMatchGateway.interface";
import { PlayerGateway } from "../gateways/playerGateway.interface";

export interface Dependencies {
    tennisMatchGateway: TennisMatchGateway,
    playerGateway: PlayerGateway
}
