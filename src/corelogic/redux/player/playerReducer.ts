import { AnyAction, combineReducers } from "redux";
import {actionTypes} from "../actionTypes";

const personalDetailsForm = (state: any = {}, action: AnyAction) => {
    if(action.type === actionTypes.RETRIEVE_PERSONAL_DETAILS_FORM_PLAYER_RETRIEVED) {
        return action.payload;
    }
    return state;
}

const fetching = (state: boolean = false, action: AnyAction) => {
    return action.type === actionTypes.RETRIEVE_PERSONAL_DETAILS_FORM_PLAYER_FETCHING;
}

export const player = combineReducers({
    personalDetailsForm,
    fetching
});