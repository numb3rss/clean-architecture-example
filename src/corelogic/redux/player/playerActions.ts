import {PersonalFormDetails} from "../../personalFormDetails.interface";
import {actionTypes} from "../actionTypes";

export function retrievePersonalDetailsFormPlayerRetrieved(retrievePersonalDetailsForm: PersonalFormDetails | undefined) {
    return { type: actionTypes.RETRIEVE_PERSONAL_DETAILS_FORM_PLAYER_RETRIEVED, payload: retrievePersonalDetailsForm };
}

export function retrievePersonalDetailsFormFetching() {
    return { type: actionTypes.RETRIEVE_PERSONAL_DETAILS_FORM_PLAYER_FETCHING };
}