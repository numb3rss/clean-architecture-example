import {AnyAction, applyMiddleware, combineReducers, createStore, Store} from "redux"
import thunk, {ThunkAction, ThunkDispatch, ThunkMiddleware} from "redux-thunk";
import {AppState} from "./appState.interface";
import {tennisMatch} from "./tennismatch/tennisMatchReducer";
import {Dependencies} from "./dependencies";
import {composeWithDevTools} from "redux-devtools-extension/index";
import { player } from "./player/playerReducer";

export const configureStore = (dependencies: Partial<Dependencies> | null) => {
    return createStore(
        combineReducers({
            tennisMatch,
            player
        }),
        composeWithDevTools(applyMiddleware(dependencies ?
            thunk.withExtraArgument(dependencies) as ThunkMiddleware<AppState, AnyAction, Dependencies> :
            thunk)));
}

export type ReduxStore = Store<AppState> &
    { dispatch: ThunkDispatch<AppState, Dependencies, AnyAction> }

export type ThunkResult<R> = ThunkAction<R, AppState, Dependencies, AnyAction>;


