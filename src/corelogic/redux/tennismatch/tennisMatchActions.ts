import { TennisMatch } from "../../tennisMatch.interface";
import { actionTypes } from "../actionTypes";

export function retrieveTennisMatchScore() {
    return { type: actionTypes.RETRIEVE_TENNIS_MATCH_SCORE };
}

export function tennisMatchScoreRetrieved(match: TennisMatch) {
    return { type: actionTypes.TENNIS_MATCH_SCORE_RETRIEVED, payload: match};
}