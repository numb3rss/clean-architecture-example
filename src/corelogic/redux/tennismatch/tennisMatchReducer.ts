import {AnyAction, combineReducers} from "redux";
import {ScoreState} from "../appState.interface";
import {actionTypes} from "../actionTypes";

const label = (state: string = '', action: AnyAction) => {
    if (action.type === actionTypes.TENNIS_MATCH_SCORE_RETRIEVED)
        return action.payload && action.payload.label;
    return state;
}

const score = (state: ScoreState = {}, action: AnyAction) => {
    if (action.type === actionTypes.TENNIS_MATCH_SCORE_RETRIEVED)
        return action.payload && action.payload.score;
    return state;
}

const fetching = (state: boolean = false, action: AnyAction) => {
    return action.type === actionTypes.RETRIEVE_TENNIS_MATCH_SCORE;
}

export const tennisMatch = combineReducers({
        label,
        score,
        fetching
    }
)
