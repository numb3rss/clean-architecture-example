export interface AppState {
    tennisMatch: TennisMatchState | null;
    player: PlayerState | null;
}

export interface TennisMatchState {
    label: string,
    score: ScoreState,
    fetching: boolean;
}

export interface PlayerState {
    personalDetailsForm: PersonalFormDetailsState,
    fetching: boolean
}

export interface ScoreState {
    [player: string]: {
        games: number,
        points: string
    }
}

export interface PersonalFormDetailsState {
    age: number,
    birthPlace: string,
    size: number,
    weight: number
}
