import React from 'react';
import ReactDOM from 'react-dom';
import './adapters/primary/index.css';
import {App} from './adapters/primary/App';
import reportWebVitals from './reportWebVitals';
import {configureStore} from "./corelogic/redux/reduxStoreConfiguration";
import {Provider} from "react-redux";
import {SmartFakeTennisGateway} from "./adapters/secondary/smartFakeTennisGateway";
import {InMemoryPlayerGateway} from "./adapters/secondary/inMemoryPlayerGateway";

const tennisMatchGateway = new SmartFakeTennisGateway();
const playerGateway = new InMemoryPlayerGateway();
playerGateway.feedData();

const store = configureStore({playerGateway, tennisMatchGateway})

ReactDOM.render(
    <React.StrictMode>
        <Provider store={store}>
            <App/>
        </Provider>
    </React.StrictMode>,
    document.getElementById('root')
);

// If you want to start measuring performance in your app, pass a function
// to log results (for example: reportWebVitals(console.log))
// or send to an analytics endpoint. Learn more: https://bit.ly/CRA-vitals
reportWebVitals();
