import {AppState} from "../../../corelogic/redux/appState.interface";

export const getTennisMatchWinner = (state: AppState): string | null => {
    if (!state.tennisMatch)
        return null;
    const score = state.tennisMatch!.score;
    for (let i = 0; i < Object.keys(state.tennisMatch!.score).length; i++) {
        const playerName = Object.keys(state.tennisMatch!.score)[i];
        if (score[playerName].games === 1)
            return playerName + " a gagné !";
    }
    return null;
}
