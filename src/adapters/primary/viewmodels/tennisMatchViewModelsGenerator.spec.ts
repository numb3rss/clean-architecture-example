import {getTennisMatchWinner} from "./tennisMatchViewModelsGenerator";

describe('Tennis match view models generator', () => {

    it('should not declare a winner if the match has not started yet', () => {
        expect(getTennisMatchWinner({tennisMatch: null})).toEqual(null);
    });

    it('should not declare a winner if the game is not finished', () => {
        const storeState = {
            tennisMatch: {
                label: 'Nadal VS Federer',
                score: {
                    'Nadal': {
                        games: 0,
                        points: '15'
                    },
                    'Federer': {
                        games: 0,
                        points: '0'
                    }
                },
                fetching: false
            },
        }
        expect(getTennisMatchWinner(storeState)).toEqual(null);
    });

    it('should declare a winner if the game is finished', () => {
        const storeState = {
            tennisMatch: {
                label: 'Nadal VS Federer',
                score: {
                    'Nadal': {
                        games: 1,
                        points: '0'
                    },
                    'Federer': {
                        games: 0,
                        points: '0'
                    }
                },
                fetching: false
            }
        }
        expect(getTennisMatchWinner(storeState)).toEqual('Nadal a gagné !');
    });

    it('should declare a winner if the game is finished', () => {
        const storeState = {
            tennisMatch: {
                label: 'Nadal VS Federer',
                score: {
                    'Nadal': {
                        games: 0,
                        points: '0'
                    },
                    'Federer': {
                        games: 1,
                        points: '0'
                    }
                },
                fetching: false
            }
        }
        expect(getTennisMatchWinner(storeState)).toEqual('Federer a gagné !');
    });

});
