import {useDispatch, useSelector} from "react-redux";
import {AppState} from "../../corelogic/redux/appState.interface";
import {getTennisMatchWinner} from "./viewmodels/tennisMatchViewModelsGenerator";
import {giveAPointToWinningPlayer} from "../../corelogic/usecases/giveAPointToWinningPlayer";
import React from "react";
import {Button, Col, Container, Row} from "react-bootstrap";
import {PersonalFormDetails} from "./PersonalDetailsForm";

export const Game = () => {
    const tennisMatch = useSelector((state: AppState) => state.tennisMatch);
    const winner = useSelector(getTennisMatchWinner);
    const dispatch = useDispatch();

    const giveAPoint = (player: number) => () => {
        dispatch(giveAPointToWinningPlayer(player));
    }

    if (tennisMatch?.fetching)
        return <p>Chargement du match...</p>

    return <Container>
        <Row className="justify-content-between mt-5 text-center">
            <Col>
                <PersonalFormDetails playerId={1}/>
                <Button onClick={giveAPoint(1)}>Nadal wins the point!</Button>
            </Col>
            <Col>
                <Row>
                    <Col>
                        <h1>{tennisMatch?.label}</h1>
                    </Col>
                </Row>
                <Row>
                    <Col className="text-center">
                        <span>{tennisMatch?.score['Nadal']?.games}</span><br/>
                        <span>{tennisMatch?.score['Nadal']?.points}</span>
                    </Col>
                    <Col className="text-center">
                        <span>{tennisMatch?.score['Federer']?.games}</span><br/>
                        <span>{tennisMatch?.score['Federer']?.points}</span>
                    </Col>
                </Row>
                {winner && <Row>
                    <Col className="text-center">
                        <span>{winner}</span>
                    </Col>
                </Row>}
            </Col>
            <Col>
                <PersonalFormDetails playerId={2}/>
                <Button onClick={giveAPoint(2)}>Federer wins the point!</Button>
            </Col>
        </Row>
    </Container>
}