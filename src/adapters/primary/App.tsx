import React from 'react';
import 'bootstrap/dist/css/bootstrap.min.css';
import './App.css';
import {Game} from "./Game";

export const App = () => {
    return <Game />
}
