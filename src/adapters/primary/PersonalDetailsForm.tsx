import {AppState} from "../../corelogic/redux/appState.interface";
import {useDispatch, useSelector} from "react-redux";
import {useEffect} from "react";
import {retrievePersonalDetailsFormPlayer} from "../../corelogic/usecases/retrievePersonalDetailsFormPlayer";

type Props = {
    playerId: number
}

export const PersonalFormDetails = (props: Props) => {
    let dispatching = false;
    const player = useSelector((state: AppState) => state.player);
    const dispatch = useDispatch();

    useEffect(() => {
        dispatch(retrievePersonalDetailsFormPlayer(props.playerId))
        console.log(player);
    }, [dispatching, props.playerId]);

    return <>
        {player?.fetching === false &&
            <>
                <h1>Profil</h1>
                <p>Age : {player?.personalDetailsForm?.age}</p>
                <p>Lieu de naissance : {player?.personalDetailsForm.birthPlace}</p>
                <p>Taille : {player?.personalDetailsForm.size}</p>
                <p>Poids : {player?.personalDetailsForm.weight}</p>
            </>
        }
    </>;
}