import {TennisGame} from "./tennisGame";

describe('Tennis match score handling', () => {

    it('should start the match with a null score on both sides', () => {
        expectScore()(0, 0)('0', '0');
    })

    describe('Game is not won yet', () => {
        it('should handle the winning points', () => {
            expectScore(1)(0, 0)('15', '0');
            expectScore(2)(0, 0)('0', '15');
            expectScore(1, 2)(0, 0)('15', '15');
            expectScore(1, 1)(0, 0)('30', '0');
            expectScore(2, 2)(0, 0)('0', '30');
            expectScore(1, 1, 1)(0, 0)('40', '0');
        });

        it('should require two points gap when deuce', () => {
            expectScore(1, 1, 1, 2, 2, 2, 1)(0, 0)('A', '40');
            expectScore(1, 1, 1, 2, 2, 2, 2)(0, 0)('40', 'A');
        });

        it('should retrograde to equality after losing an advantage',() => {
            expectScore(1, 1, 1, 2, 2, 2, 1, 2)(0, 0)('40', '40');
            expectScore(1, 1, 1, 2, 2, 2, 2, 1)(0, 0)('40', '40');
        });

    });

    describe('A player won a game',() => {
        it('should increase his game score and resetting score points', () => {
            expectScore(1, 1, 1, 1)(1, 0)('0', '0');
            expectScore(2, 2, 2, 2)(0, 1)('0', '0');
            expectScore(1, 2, 2, 2, 2)(0, 1)('0', '0');
            expectScore(1, 1, 1, 2, 1)(1, 0)('0', '0');
        });

        it('huge game - acceptance test', () => {
            expectScore(1, 1, 1, 2, 2, 2, 2, 1, 2, 1, 2, 1, 2, 1, 1)(0, 0)('A', '40');
        });

    });

    const expectScore = (...winningPlayers: number[]) =>
        (player1Game: number, player2Game: number) =>
        (player1Points: string, player2Points: string) => {
        const tennisMatch = new TennisGame();
        winningPlayers.forEach(p =>  tennisMatch.handleWinningPoint(p));
        expect(tennisMatch.score()).toEqual(
            {
                p1: {game: player1Game, points: player1Points},
                p2: {game: player2Game, points: player2Points}
            });
    };
});
