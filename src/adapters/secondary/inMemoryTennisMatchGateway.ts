import {TennisMatchGateway} from "../../corelogic/gateways/tennisMatchGateway.interface";
import {TennisMatch} from "../../corelogic/tennisMatch.interface";

export class InMemoryTennisMatchGateway implements TennisMatchGateway {

    private tennisMatch: TennisMatch | null = null;
    private _justWonPlayer: number | null = null;

    giveAPoint(player: number): Promise<TennisMatch> {
        this._justWonPlayer = player;
        return Promise.resolve(this.tennisMatch!);
    }

    feedWithTennisMatch(tennisMatch: TennisMatch) {
        this.tennisMatch = tennisMatch;
    }

    get justWonPlayer(): number | null {
        return this._justWonPlayer;
    }
}
