import {PlayerGateway} from "../../corelogic/gateways/playerGateway.interface";
import {PersonalFormDetails} from "../../corelogic/personalFormDetails.interface";
import {PersonalFormDetailsEntity} from "./personalFormDetailsEntity.interface";

export class InMemoryPlayerGateway implements PlayerGateway {
    private personalFormsDetails: PersonalFormDetailsEntity[] = [];

    retrievePersonalFormDetails(playerId: number): Promise<PersonalFormDetails | undefined> {
        const personalFormDetails: PersonalFormDetailsEntity | undefined = this.personalFormsDetails.find((p: any) => p.id === playerId);
        if(personalFormDetails === undefined) return Promise.resolve(undefined)
        return Promise.resolve({
            age: personalFormDetails.age,
            birthPlace: personalFormDetails.birthPlace,
            size: personalFormDetails.size,
            weight: personalFormDetails.weight
        });
    }

    feedData() {
        this.personalFormsDetails = [
            {
                id: 1,
                age: 34,
                birthPlace: 'Manacor, Espagne',
                size: 1.85,
                weight: 84
            },
            {
                id: 2,
                age: 39,
                birthPlace: 'Bâle, Suisse',
                size: 1.85,
                weight: 84
            }
        ]
    }

}