import {TennisMatchGateway} from "../../corelogic/gateways/tennisMatchGateway.interface";
import {TennisGame} from "./tennisGame";
import {TennisMatch} from "../../corelogic/tennisMatch.interface";

export class SmartFakeTennisGateway implements TennisMatchGateway {

    private tennisGame: TennisGame;

    constructor() {
        this.tennisGame = new TennisGame();
    }

    giveAPoint(player: number): Promise<TennisMatch> {
        this.tennisGame.handleWinningPoint(player);
        const score = this.tennisGame.score();
        return Promise.resolve({
            label: 'Nadal - Federer',
            score: {
                'Nadal': {
                    games: score.p1.game,
                    points: score.p1.points
                },
                'Federer': {
                    games: score.p2.game,
                    points: score.p2.points
                }
            }
        });
    }

}
