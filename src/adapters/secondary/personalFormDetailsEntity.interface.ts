export interface PersonalFormDetailsEntity {
    id: number,
    age: number,
    birthPlace: string,
    size: number,
    weight: number
}