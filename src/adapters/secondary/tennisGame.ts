export class TennisGame {

    private player1Points: number = 1;
    private player2Points: number = 1;
    private p1HasWonAPoint = false;
    private p2HasWonAPoint = false;
    private pointsTypes = ['A', '0', '15', '30', '40', '0'];

    handleWinningPoint(winner: number) {
        if (winner === 1) {
            this.p1HasWonAPoint = true;
            this.addPointToPlayer(1);
        } else {
            this.p2HasWonAPoint = true;
            this.addPointToPlayer(2);
        }
        if(this.gameHasBeenWon()) {
            this.player1Points = 1;
            this.player2Points = 1;
            if(winner === 1)
                this.p2HasWonAPoint = false;
            else
                this.p1HasWonAPoint = false;
        }
    }

    private addPointToPlayer(winner: number) {
        if (this.isDeuce())
            this.setAdvantage(winner);
        else if(winner === 1) {
            if (this.player2Points === 0) {
                this.player2Points = 4
            } else
                this.markPoint(winner);
        } else if (this.player1Points === 0) {
            this.player1Points = 4;
        } else
            this.markPoint(winner);
    }

    private markPoint(winner: number) {
        if (winner === 1)
            this.player1Points += 1;
        else
            this.player2Points += 1;
    }

    private setAdvantage(winner: number) {
        if (winner === 1)
            this.player1Points = 0;
        else
            this.player2Points = 0;
    }

    score() {
        const {p1Games, p2Games} = this.scoreGames();
        const {p1Points, p2Points} = this.scorePoints();
        return {
            p1: {game: p1Games, points: p1Points},
            p2: {game: p2Games, points: p2Points},
        }
    }

    private scoreGames() {
        return {
            p1Games: this.p1HasWonAPoint && this.pointsTypes[this.player1Points] === '0' ? 1 : 0,
            p2Games: this.p2HasWonAPoint && this.pointsTypes[this.player2Points] === '0' ? 1 : 0,
        }
    }

    private gameHasBeenWon() {
        return this.p1HasWonAPoint && this.pointsTypes[this.player1Points] === '0' ||
        this.p2HasWonAPoint && this.pointsTypes[this.player2Points] === '0';
    }

    private scorePoints() {
        return {
            p1Points: this.pointsTypes[this.player1Points],
            p2Points: this.pointsTypes[this.player2Points],
        }
    }

    private isDeuce() {
        return this.player1Points === 4 && this.player2Points === 4;
    }
}
